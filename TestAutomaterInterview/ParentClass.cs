﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestAutomationInterview
{
    abstract class ParentClass
    {
        abstract public double anonymousArithmeticFunction(double argument1, double argument2);
    }
}
