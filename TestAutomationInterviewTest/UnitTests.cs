﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;
using TestAutomationInterview;

namespace TestAutomationInterviewTest
{
    [TestFixture]
    public class UnitTests
    {
        [TestFixture]
        public class Class1
        {
            private static readonly TextWriter oldOut = Console.Out;
            private static readonly string newline = Environment.NewLine;
            private static StringWriter newOut;

            [SetUp]
            public void testSetup()
            {
                newOut = new StringWriter();
                Console.SetOut(newOut);
            }

            [TearDown]
            public void testTeardown()
            {
                Console.SetOut(oldOut);
                newOut = null;
            }

            [Test]
            public void unitTest001()
            {
                Assert.That(BooleanGenerator.GetBoolean(), Is.EqualTo(true));
            }

            [Test]
            public void unitTest002()
            {
                TestAutomationInterview.HelloWorld.executeMain(null);
                Assert.That(newOut.ToString(), Is.EqualTo("Hello World!" + newline));
            }

            [Test]
            public void unitTest003()
            {
                var maxInt = new Random().Next(200);
                TestAutomationInterview.FizzBuzz.executeMain(new string[] { maxInt.ToString() });
                Assert.That(newOut.ToString(), Is.EqualTo(TestUtilities.getFizzBuzzOutput(maxInt)));
            }

            [Test]
            public void unitTest004()
            {
                var maxInt = new Random().Next(200, 400);
                TestAutomationInterview.FizzBuzz.executeMain(new string[] { maxInt.ToString() });
                Assert.That(newOut.ToString(), Is.EqualTo(TestUtilities.getFizzBuzzOutput(maxInt)));
            }

            [Test]
            public void unitTest005()
            {
                var maxInt = new Random().Next(20, 40);
                var recursiveSum = TestAutomationInterview.Recursion.sumOfIntegersFrom1till(maxInt);
                var iterativeSum = TestUtilities.sumOfIntegersFrom1till(maxInt);
                Assert.That(recursiveSum, Is.EqualTo(iterativeSum));
            }

            [Test]
            public void unitTest006()
            {
                List<int> intlist = TestUtilities.generateListOfRandomNumbers(0, 1000, 100);
                var recursiveMinimum = TestAutomationInterview.Recursion.minimumElementIn(intlist);
                var iterativeMinimum = TestUtilities.minimumElementIn(intlist);
                Assert.That(recursiveMinimum, Is.EqualTo(iterativeMinimum));
            }

            [Test]
            public void unitTest007()
            {
                var argument1 = 2d;
                var argument2 = 10d;
                ParentClass child = getParentReference(new ChildClass());
                var result = child.anonymousArithmeticFunction(argument1, argument2);
                Assert.That(result, Is.EqualTo(1024d));
            }

            [Test]
            public void unitTest008()
            {
                var argument1 = 3d;
                var argument2 = 3d;
                ParentClass child = getParentReference(new ChildClass());
                var result = child.anonymousArithmeticFunction(argument1, argument2);
                Assert.That(result, Is.EqualTo(27d));
            }

            static ParentClass getParentReference(object o)
            {
                if(o is ParentClass)
                {
                    ParentClass parentRef = (ParentClass)o;
                    return parentRef;
                }
                return null;
            }
        }
    }
}
