﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestAutomationInterviewTest
{
    class TestUtilities
    {
        internal static string getFizzBuzzOutput(int maxInt)
        {
            var stringBuilder = new StringBuilder();
            for(int i=1; i<=maxInt; i++)
            {
                stringBuilder.AppendLine(printFizzBuzzStyle(i));
            }
            return stringBuilder.ToString();
        }

        internal static string printFizzBuzzStyle(int i)
        {
            if (i % 15 == 0)
            {
                return "FizzBuzz";
            }
            else if (i % 5 == 0)
            {
                return "Buzz";
            }
            else if (i % 3 == 0)
            {
                return "Fizz";
            }
            else
            {
                return i.ToString();
            }
        }

        internal static int sumOfIntegersFrom1till(int maxInt)
        {
            var sum = 0;
            for(int i=1;i<= maxInt;i++)
            {
                sum += i;
            }
            return sum;
        }

        internal static List<int> generateListOfRandomNumbers(int min, int max, int listcount)
        {
            List<int> intlist = new List<int>();
            Random rnd = new Random();
            for (int i = 0; i < listcount; i++)
            {
                intlist.Add(rnd.Next(min, max));
            }
            return intlist;
        }

        internal static int minimumElementIn(List<int> intlist)
        {
            int minimum = intlist[0];
            foreach(int nr in intlist)
            {
                minimum = (nr < minimum) ? nr : minimum;
            }
            return minimum;
        }
    }
}
